"""Testing the bot."""
import sys
from os import getenv
import discord  # type: ignore
from discord import Client, Message, TextChannel, Webhook, AsyncWebhookAdapter
from dotenv import load_dotenv
from rich.console import Console
import aiohttp
import logging
from rich.logging import RichHandler
from rich.panel import Panel
from rich.spinner import SPINNERS
from random import choice
import pyfiglet  # type: ignore
import asyncio


console = Console()
logging.basicConfig(
    level="INFO", format="%(message)s", datefmt="[%X]", handlers=[RichHandler(console=console, markup=True)]
)

logger = logging.getLogger("rich")

load_dotenv()
client = discord.Client()

def spinner() -> str:
    return choice(list(SPINNERS.keys()))


status = console.status('[bold green]Starting bot...', spinner=spinner())


def fmt_version(version: tuple) -> str:
    return '.'.join(map(str, list(version)[:3]))


@client.event
async def on_ready() -> None:
    global status
    logger.info('We have logged in as %s', client.user)
    status.stop()
    del status


@client.event
async def on_message(message: Message) -> None:
    global status
    if message.author.bot:
        logger.info('[bold]Ingored bot.[/]')
        return

    logger.info(f'[bold yellow]New message ![/]')
    channel: TextChannel = message.channel
    logger.info(f'[magenta]{message.author}@{channel}[/] :')
    logger.info(message.content, extra={'markup': False})

    if channel.id == int(getenv('CHANNEL_ID') or 0):
        logger.info('Message in the right channel')

    logger.info('[bold yellow]Succesfully treated message ![/]')


def main():
    try:
        if not getenv('BOT_TOKEN'):
            raise EnvironmentError('BOT_TOKEN not set')
        if not getenv('CHANNEL_ID'):
            raise EnvironmentError('CHANNEL_ID not set')
        if not getenv('WEB_HOOK_URL'):
            raise EnvironmentError('WEB_HOOK_URL not set')
        logger.info('Env vars ok, connecting')
        client.run(getenv('BOT_TOKEN'))
    except AttributeError:
        logger.error('Connexion timeout...')
    except:
        console.print_exception(show_locals=True)
    finally:
        logger.info('Stopping bot...')
        try:
            status.stop()
        except:
            pass
        asyncio.run(client.close())


if __name__ == '__main__':
    title = pyfiglet.figlet_format('Discord Threads', font='slant')
    console.print(Panel.fit(f'[bold red]{title}[/]'))
    status.start()
    logger.info(f'Running on python version {fmt_version(sys.version_info)}')
    logger.info(f'With a discord.py version {fmt_version(discord.version_info)}')
    main()
